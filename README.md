# Visual Novel System

A [Solarus Game Engine](https://www.solarus-games.org/) addon that allows for massive customization options for dialogs via config files.

## Installation

1. Clone the repository
2. Open Solarus Quest Editor
3. Open the quest you wish to add VNS to
4. Select `File` -> `Import from a quest`
5. Navigate to the VNS repo you cloned
6. Import the `data/scripts` directory from VNS to your projects scripts directory
7. Open `data/scripts/dialogs/configs/default.lua` file in your quest.
8. Find the `dialogs.text.line.font` key (it's value is `8_bit` by default)
9. Replace with a font name from your `data/fonts` directory

### Integrating VNS With Existing Solarus Projects

If your game already has a `scripts/features.lua` file don't import it. It will overwrite yours. Instead add `require("scripts/dialogs/visual_novel_manager")` on a new line in your `scripts/features.lua` instead.

If your game has a `scripts/multi_events.lua` delete it and replace it with the `scripts/multiple_events.lua` file included in this project. Remember to update your project's require statements. The `scripts/multiple_events.lua` is required for VNS to work.

## Usage

Due to sheer amount of possible customizations available. It's impractical to list them all here.

Please check the `documentation` folder for detailed information about everything the Visual Novel Dialog System can do. Each section has examples and images to help illustrate what's happening.

For the full list of VNS Configs check the [config_hierarchy](documentation/config_hierarchy.md)

For details around multiple langague support check the [multiple_languages_guide](documentation/multiple_languages_guide.md)

Here's a few examples of the Visual Novel System in action (Note if any of the .gif animations are not playing then click on them):
![Fade in and Slide](documentation/screenshots/transitions/fade_and_slide.gif)

![Example Icon Question And Sprite Answer](documentation/screenshots/question/icon_and_animated.gif)

![Example Display With Images](documentation/screenshots/midline/dialog_with_images.png)

![Example Display With Sprites](documentation/screenshots/midline/dialog_with_sprites.gif)

## Example

For a project using the Visual Novel Dialog System Check out the [Knights Demo Game](https://gitlab.com/ShargonPendragon/knights-demo-game)

## Contributing

- Fork The Repo
- Make Changes
- Test Locally
- Submit PR
