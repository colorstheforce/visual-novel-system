# Midline_Options

Midline options allow you to customize your dialog text on a line by line basis.
Everything from color, to size. To even adding sprites and images in the middle of the line.

## Applicable Objects

- [line](line.md)

## Flags

Flags are special words inside your dialog to tell the Visual Novel System to change something. They all follow this syntax:
`$FLAG_NAME=FLAG_VALUE$`

Any property of a solarus text surface can be specified as a flag. So please check the [Solarus Text Surface documentation](https://www.solarus-games.org/doc/latest/lua_api_text_surface.html#lua_api_text_surface_functions) for more information.

With that being said the most commonly used flags are:

- font
- font_size
- color

Simply add these flags to your dialog and they should take effect immediately:

![Example Dialog With Font Flags](screenshots/midline/example_font_flag.png)

![Example Display With Font Changed](screenshots/midline/dialog_with_font_changed.png)

## Graphic Flags

There are two additional flags that the Visual Novel System can use. They allow for adding midline images or sprites. Please check out the below examples on how to use them.

### Image Flag Example

Add an [image](image.md) config under the `line` element. But before you do please be aware of these slight changes:

- Do NOT specify position (it's already relative to the line)
- x and y offsets apply to ALL images on the line. So it's best that they are all the same size
- `path` points to the DIRECTORY we are going to load the images from.

Here an example image config:

```lua
dialog_box = {
  text = {
    line = {
      image = {
        path = "demo/hud/cursor/icons/individual_icons",
        x_offset = 0,
        y_offset = -8
      }
    }
  }
}
```

![Example Dialog With Image Flags](screenshots/midline/example_image_flag.png)

![Example Display With Images](screenshots/midline/dialog_with_images.png)

### Sprite Flag Example

Add a sprite config under the `line` element. NOT under image. As you see elsewhere in the Visual Novel System.

All the caveats that apply to images also apply to sprites.

In addition to the attributes of images you can also apply an `animation` and `direction` if they are different then the default.

NOTE: If you do specify an animation and/or direction ALL sprites loaded must have those defined.

sprite config:

```lua
dialog_box = {
  text = {
    sprite = {
      path = "demo/hud/cursor/sprites/",
      animation = "confirmation",
      direction = 0,
      x_offset = 15,
      y_offset = 0
    }
  }
}
```

![Example Dialog With Sprite Flag](screenshots/midline/example_sprite_flag.png)

![Example Display With Sprites](screenshots/midline/dialog_with_sprites.gif)

NOTE: The extra spacing on either side of the sprite is the result of the sprite being a bit bigger than the image. I'm just a bad artist.
