# Characters

characters are one of the more involved parts of the Visual Novel Dialog System. So instead of throwing a bunch of configs at you. Instead I'm going to walk you through each step of character customization from easiest to hardest.

## Adding characters to dialog

Before we get in to any sort of customization we first have to tell the Visual Novel Dialog System what characters are speaking. We do that via these two special flags: `@` and `===` Here's a small example of two characters talking:

![Dialog Layout](screenshots/characters/dialog_layout.png)

In this example we have 2 characters `Sir Silver` and `Lyons`. Character Names can have spaces but MUST only be on the same line as `@` and you must have an `===` symbol on it's own line to denote when that character is done speaking.

Just by doing that. The Visual Novel System will automatically add their names to the screen.

This gif already has a dialog and name_box setup but has no other customization applied:

![Names Added](screenshots/characters/names_added.gif)

## Localized Names

However we now have an issue. We receive a warning message in our console.

![Non Localized Name Warning Image](screenshots/characters/non_localized_name_warning.png)

The reason is because we haven't given a localization for that name. Solarus handles this by having a `strings.dat` for each language. Which contains localization options for that language. You can check out more information on the strings.dat file here: [Solarus Docs](https://www.solarus-games.org/doc/latest/quest_language_strings.html)

You can add the missing key like so:

![Solarus Editor Strings](screenshots/characters/solarus_editor_strings.png)

Notice that I've set the value to `Silver Knight`

and viola!

![Localized Character Name](screenshots/characters/localized_character_name.png)

## Characters Configs

Character configs are under the `characters` directory and, like scene configs, the `default.lua` in the characters directory is always loaded before any character configs are. Here's an example of some characters:

![Directory Layout](screenshots/characters/directory_layout.png)

The name used in the dialogs MUST match the character.lua name. So in the example above. To use the `silver_knight.lua` You would layout your dialog like so:

![With Character Config](screenshots/characters/dialog_with_character_config.png)

## Character Configs

A basic character config just has information about the character image. Which is laid out exactly as the [image config](image.md). In the example below. I've updated the `silver_knight.lua` config to just have a character image appear on the right side of the screen.

```lua
image = {
  path = "demo/visual_novel_art/characters/silver_knight/SilverKnightSingle.png",
  position = "right",
}
```

![Single Character](screenshots/characters/single_character.png)

But what if we wanted to get a little more complicated? We can have our character config override the scene settings. In the config below we override the scene so that when silver_knight is speaking their portrait appears on the outside right of the dialog box. And to prevent the normally large dialog box from pushing his portrait off screen the dialog_box has been replaced with a smaller version and has moved its' position to left to give the portrait additional room.

```lua
image = {
  path = "demo/visual_novel_art/characters/portaits/SilverKnightPortrait.png",
  position = "outsideright",
  relative_to_dialog_box = true,
},
dialog_box = {
  image = {
    position = "left",
    path = "demo/hud/dialog_boxes/smaller_dialog_box.png"
  }
}
```

![Character Config Override](screenshots/characters/character_config_override_scene.gif)

characters as a general rule have a higher precedence than the scene. And they can modify ANY scene object. (backgrounds, dialog boxes, name_boxes etc.) The only thing they can't affect are [midline options](midline_options.md), and other characters. There is an exception to this rule that we'll talk about later.

But what happens when two characters talk? The current speaker has priority and their changes will override the scene:

![Character Override Character](screenshots/characters/character_config_override_character.gif)

Notice that you can still see the character portrait off on the right? That's because while the scene changed. The character portrait automatically adjusted to be on the outside right of the new dialog box. Remember that characters can't directly affect other character's placement. So you need to consious of where you are placing your characters.

## Overriding Character Configs

If you read the [Scene Documentation](scene.md) you probably saw that character configs have less precedence then scene configs. But I said above that characters have precedence. So which is it?

The answer is: Both.

All configs are eventually rolled up into a single giant config. And all character configs are rolled up into an element called `characters` and each character has their configs under their name. And those configs have precedence. But if you know what characters are talking in a scene you can add them in your scene config.

Let me give you an example:
Let's use the `silver_knight.lua` config from before:

```lua
image = {
  path = "demo/visual_novel_art/characters/portaits/SilverKnightPortrait.png",
  position = "outsideright",
  relative_to_dialog_box = true,
}

dialog_box = {
  image = {
    position = "left",
    path = "demo/hud/dialog_boxes/smaller_dialog_box.png"
  }
}
```

Now when all of the characters are loaded for the scene. The scene config looks like this:

```lua
characters = {
  silver_knight = {
    image = {
      path = "demo/visual_novel_art/characters/portaits/SilverKnightPortrait.png",
      position = "outsideright",
      relative_to_dialog_box = true,
    },
    dialog_box = {
      image = {
        position = "left",
        path = "demo/hud/dialog_boxes/smaller_dialog_box.png"
      }
    }
  }
}

background = {
  image = {
    path = "path/to/my/cool/background"
  }
}

dialog_box = {
  close_delay = 0,
  image = {
    position = "center",
    path = "demo/hud/dialog_boxes/dialog_box.png"
  }
}
```

See how all of silver_knight's config got rolled up under the `characters` element? So what that allows you to do. Is in your scene `config.lua` you could add:

```lua
characters = {
  silver_knight = {
    image = {
      position = "left"
    }
  }
}
```

And for that scene (and any of it's children provided they don't override it). silver_knight will appear on the left. Just like any other element.

## Character Image Override From Dialog

You'll notice that other Visual Novel Systems like Ren'Py allow you change the character image from the dialog like so `John happy` We allow for something similiar. Here's how it works:

Say we wish to change our silver_knight to a green image when they're feeling envy. First we go to our character config and add a new table called `emotions` to it.

Here's an example of the table:

```lua
emotions = {
  angry = {
    image = {
      path = "red_knight/RedKnight.png",
      position = "left",
      x_offset = 0,
      y_offset = 0
    }
  },
  envy = {
    image = {
      path = "green_knight/GreenKnight.png",
      position = "left",
      x_offset = 0,
      y_offset = 0
    }
  }
}
```

Under the `emotions` key are the emotions that you want to set up. These strings can be used in the dialogs. The two emotions that are setup in the example above are `angry` and `envy`.

Assuming your paths are right then the only thing left to do is use those flags in your dialog like so:

```text
@silver_knight_3 (angry)
I feel $color=red$ ANGRY!!!!
Ugh Why do I have to override
my stupid image config on a scene by scene basis
===

@silver_knight_3 (envy)
I so $color=green$ joalous $color=white$ of those other
visual novel systems where they can override
characters straight from their dialog without having
to deal with a bunch of config files
===

@silver_knight_3
Oh wait! We totally can do that too!
Just type @MY_NAME (EMOTION)
in your dialog! You can setup your character config
to override the images based on that emotion.
===
```

And the result:

![Character Override Character](screenshots/characters/character_emotion.gif)

Note our last knight reverted back to the inital knight color and position of that character config.

## Final Notes

There is no limit imposed by the Visual Novel System about how many characters or how many lines, or how often they can speak In a single dialog.

As with most other components none of this is mandatory. If you do not specify any characters then only the `characters/default.lua` information will be used. And if you leave it empty then no character config information will be used at all.
