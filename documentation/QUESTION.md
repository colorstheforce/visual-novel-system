# Question

If at least 2 displayed lines in a dialog box start with a question marker.
The text is considered to contain a question. A question box can have additional
options applied to it which are contained here

## Applicable Objects

- [text](text.md)

## Contains Objects

- [image](image.md)

## How To Use In Script File

A question box differs from a normal dialog box in that it has a return an answer.
An answer is the LINE NUMBER THAT THE ANSWER IS ON. Not the answer itself.

So for example say we have the following box:

```text
How do you like your eggs?
$? Over Easy
$? Scrambled
```

Line 1: How Do You Like Your Eggs
Line 2: Over Easy
Line 3: Scrambled

If we select Over Easy the Answer value we return is 2. If we select Scrambled the
answer value is 3.

Here's an example of how we would ask a question in a lua script.

```lua
game:start_dialog("dialog.id", function(answer) -- start the question dialog with a function callback
  if answer == 2 then -- Since Over Easy is line 2 we check if that's what we answered.
    print("Over Easy Eggs are superior") -- Ditto for Scrambled Eggs on line 3
  elseif answer == 3 then
    print("Scrambled Eggs are the BEST!")
  end
end)
```

## Attributes

|name|default|values|description|
|----|-------|------|-----------|
|line_buffer| 4| Any Positive Integer| How many spaces to add to the start of the line (to leave enough room fo the cursor)
|question_marker|$?| Any String| What question strings should start with|
|cursor_wrap|true|boolean|Allow the cursor to wrap (i.e. if pressing up from top option move cursor to bottom option and vice versa)|
|move_sound| | |path to sound to play when cursor is moved (relative to sounds directory)
|selection_sound| | |path to sound to play when answer is selected (relative to sounds directory)
|cursor| | |see Cursor section|
|answer| | |see Answer section|

### Cursor

A cursor is an image used to tell the player what choice they are currently on. It can be specified one of 3 ways. The first two are a static image and a sprite. Further details on them can be found [here](image.md)

The last one allows you to load a single image from a larger sheet. e.g. if you
had a sheet full of icons and wanted to use just one.

In order to do that you must specify the cursor width and height. We use those
to cut up the sprite sheet into a grid where each cell is that width and height
of the image. Then you must supply the row and column of the image you wish to use.
This is meant to be used when you have a sheet of icons laid out on a grid. And
want to use one without having to cut up the sheet into a bunch of single icons.

### Answer

Follows the exact same format as the [cursor](###Cursor). It is a confirmation
cursor. e.g. Changing the cursor to a thumbs up after the player makes a selection.
However unless there is some sort of delay. The box will close before you even
see it.
You can either set a delay via the [dialog box](dialog_box.md) or [transitions](transitions.md) (if you are using them)

## Examples

### cursor loaded from a spritesheet. Answer cursor is a sprite

```lua
dialog_box = {
  close_delay = 1000,
  text = {
    question = {
      cursor = {
        image = {
          path = "demo/hud/cursor/icons/spritesheet.png",
          icon = {
            width = 16,
            height = 16,
            row = 7,
            column = 2
          }
        }
      },
      answer = {
        image = {
          path = "demo/hud/cursor/sprites/ball_shrunk",
          sprite = {
            animation = "confirmation"
          }
        }
      }  
    }
  }  
}
```

![Example Icon Question And Sprite Answer](screenshots/question/icon_and_animated.gif)

### cursor is an image

notice that we didn't have to specify a close delay because there is no answer cursor.

```lua
dialog_box = {
  text = {
    question = {
      cursor = {
        image = {
          path = "demo/hud/cursor/icons/individual_icons/book_02e.png",
          y_offset = -8,
          x_offset = 0
        }
      }
    }
  }
}
```

![Example Image Cursor](screenshots/question/image_cursor.gif)
