ignore = {
  "sol", -- This is a solarus global varaible we don't define it. 
  "212/self", -- self is pasted by the solarus engine so we don't define it.
  "432/self" -- self is pasted by the solarus engine.
}

exclude_files = {"data/scripts/dialogs/configs/*"}

max_line_length = 160